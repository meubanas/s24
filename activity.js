db.users.insertMany([
	{
		firstName: "Nikola",
		lastName: "Tesla",
		age: 64,
		department: "Communication"
	},
	{
		firstName: "Marie",
		lastName: "Curie",
		age: 28,
		department: "Media and Arts"
	},
	{
		firstName: "Charles",
		lastName: "Darwin",
		age: 51,
		department: "Human Resources"
	},
	{
		firstName: "Rene",
		lastName: "Descartes",
		age: 50,
		department: "Media and Arts"
	},
	{
		firstName: "Albert",
		lastName: "Einstein",
		age: 51,
		department: "Human Resources"
	},
	{
		firstName: "Michael",
		lastName: "Faraday",
		age: 30,
		department: "Communication"
	}

])

// find user letter s in first name or d in last name and show firstname and lastname
db.users.find(
	{
		$or:
		 [
		 	{
		 		firstName: {$regex: 's'}
		 	},
		 	{
		 		lastName: {$regex: 'd'}
			}
		 ]
	},
	{
		firstName:1, lastName:1, _id:0
	}
);

// find user from Human Resources Department age is greater than or equal 50

db.users.find({$and: 
		[
			{
				department: "Human Resources"
			}, 
			{
				age: {$gte: 50}
			}
		]
	});

// find users with letter e in the firstname and age less than 30

db.users.find(
	{
		$and:
		 [
		 	{
		 		firstName: {$regex: 'e', $options: '$e'}
		 	},
		 	{
		 		age: {$lte: 30}
			}
		 ]
	}
);


